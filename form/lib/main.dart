import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Forms"),
          centerTitle: true,
          backgroundColor: Colors.red,
          actions: [
            IconButton(
                icon: Icon(
                  Icons.search,
                  color: Colors.white,
                ),
                onPressed: () => {print("Clicked search")})
          ],
        ),
                body: SingleChildScrollView(
                                  child: Column(
               
                    children: [
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: TextFormField(
                        
                        decoration: InputDecoration(
                          icon: Icon(Icons.face),
                          labelText: "Enter your name",
                          border:OutlineInputBorder(),
                          
                        ),
                      ),
                    ),
                               Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          labelText: "Enter Email",
                          icon: Icon(Icons.email),
                          
                          border:OutlineInputBorder(),
                          
                        ),
                      ),
                    ),
                                        Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: TextFormField(
                       
                        keyboardType:TextInputType.number,
                        obscureText: true,
                        decoration: InputDecoration(
                          icon: Icon(Icons.keyboard),
                          labelText: "Password",
                          border:OutlineInputBorder(),
                          
                        ),
                      ),
                    ),
                     Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: TextFormField(
                       
                        keyboardType:TextInputType.number,
                    
                        decoration: InputDecoration(
                          icon: Icon(Icons.phone),
                          labelText: "Ph no:",
                          border:OutlineInputBorder(),
                          
                        ),
                      ),
                    ),
                  ],),
                ),

        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              UserAccountsDrawerHeader(
                accountName: Text("Samuel"),
                accountEmail: Text("iamsamuelhere@gmail.com"),
                currentAccountPicture: CircleAvatar(
                  backgroundImage: AssetImage('images/sam.png'),
                ),
              ),
              ListTile(
                leading: Icon(Icons.face, color: Colors.blue),
                title: Text("Name"),
                subtitle: Text("Samuel"),
                trailing: Icon(Icons.favorite, color: Colors.red),
              ),
              ListTile(
                leading: Icon(Icons.email, color: Colors.blue),
                title: Text("Email"),
                subtitle: Text("iamsamuelhere@gmail.com"),
                trailing: Icon(Icons.favorite, color: Colors.red),
              ),
              ListTile(
                leading: Icon(Icons.baby_changing_station, color: Colors.blue),
                title: Text("Gender"),
                subtitle: Text("Male"),
                trailing: Icon(Icons.favorite, color: Colors.red),
              ),
              ListTile(
                leading: Icon(Icons.home, color: Colors.blue),
                title: Text("Home"),
                subtitle: Text("Bangalore"),
                trailing: Icon(Icons.favorite, color: Colors.red),
              ),
              Divider(thickness: 1.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                      icon: Icon(Icons.favorite, color: Colors.blue),
                      onPressed: () => {}),
                  Text("Flutter Developer")
                ],
              )
            ],
          ),
        ),
     floatingActionButton: FloatingActionButton.extended(
       backgroundColor: Colors.green,
       onPressed: ()=>{},icon:Icon(Icons.check),label:Text("send"),
      ),
     )
      );
    
  }
}
